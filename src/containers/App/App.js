import React, {useState} from 'react'
import './App.css';
import Modal from "../../components/UI/Modal/Modal";
import ButtonComponent from "../../components/UI/Button/ButtonComponent";
import Alert from "../../components/UI/Alert/Alert";

const App = () => {
    const [show, setShow] = useState(false);

    const [alert, setAlert] = useState(true);


    return (
        <>
            <ButtonComponent click={() => setShow(true)} title={'Show Modal'}/>
            <div className="App">
                <Modal
                    show={show}
                    closeModal={() => setShow(false)}
                    title="Some kinda modal title"
                />
            </div>
            <Alert
                show={alert}
                type="warning"
                title={'This is a warning type alert!'}
                close={() => setAlert(false)}
            >
                <div>
                    <p>Warning</p>
                </div>
            </Alert>
            <Alert show={alert}
                   type="success"
                   title={'This is a success type alert!'}
                   close={() => setAlert(false)}>
                <div>
                    <p>success</p>
                </div>
            </Alert>
        </>
    )
};

export default App;
