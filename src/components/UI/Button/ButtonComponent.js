import React from 'react';
import './ButtonComponents.css';
const ButtonComponent = (props) => {
    return (
        <button className="btn-show" onClick={props.click}>{props.title}</button>
    );
};

export default ButtonComponent;