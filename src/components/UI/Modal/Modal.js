import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {

    return (
        <>
            <Backdrop show={props.show} onClick={props.closed}/>
            <div
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <h2>{props.title} <span><button onClick={props.closeModal}>&times;</button></span></h2>
                <hr/>
                <p>This is modal content</p>

            </div>
        </>
    );
};

export default Modal;