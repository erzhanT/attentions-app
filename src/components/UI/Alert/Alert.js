import React from 'react';
import './Alert.css'
const Alert = props => {
    return (
        <div
            className={['Alert', props.type].join(' ')}
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
        >
            <p>{props.title} <span><button onClick={props.close}>Close</button></span></p>

        </div>

    );
};

export default Alert;